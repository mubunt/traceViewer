# RELEASE NOTES: *traceViewer*, a generic and customizable tabular viewer for traces

Functional limitations, if any, of this version are described in the *README.md* file.


- **Version 2.0.3**:
  - Updated build system components.

- **Version 2.0.2**:
  - Updated build system.

- **Version 2.0.1**:
  - Removed unused files.

- **Version 2.0.0**:
  - Updated user interface with a dark one.


- **Version 1.4.4**:
  - Updated build system component(s)

- **Version 1.4.3**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.4.2**:
  - Updated SWT library from 4.12 to 4.15.

- **Version 1.4.1**:
  - Removed build method for Windows and associated files.

- **Version 1.3.1**:
  - Some minor changes in project structure and build.

- **Version 1.3.0**:
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Removed *index.html* file (was used for *GitHub*).
  - Moved from GPL v2 to GPL v3.
  - Moved to SWT 4.12. Initially was 4.6.3.
  - Improved ./Makefile, ./registerView_bin/Makefile, ./registerView_c/Makefile, ./registerView_java/Makefile.
  - Removed C compilation warnings.
  - Added ".comment" file in each directory for 'yaTree' utility.
  - Updated README file.

- **Version 1.2.1**:
  - Fixed typos and omission in README file.

- **Version 1.2.0**:
  - Driver is now a C executable. Was a script (bash / cmd) file.
  - Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).
  - Specified foreground and background colors after migration from Ubuntu/Mate
	  to XUbuntu/Xfce4 (slightly differents with both 2 window managers).
  - Move to SWT 4.6.3. Previously was 4.6.2.
  - Move to Commons CLI 1.4 Previously was 1.3.1.
  - Move to Commons EXEC 1.3. Previously was 1.2

- **Version 1.1.0**:
  - Moved to SWT (Standard Widget Toolkit) 4.6.2 (previous: 4.6.1).
  - Moved from ECLIPSE Neon.1 Release (4.6.1)  to IntelliJ IDEA 2016.3.2.

- **Version 1.0.0**:
  - Fixed typos, indentation, licensing

- **Version 1.0.0**:
  - First release.

--- End of Document ------------------------------------------------------------