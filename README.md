
# APPLICATION: *traceViewer*

From Wikipedia, the free encyclopedia: In software engineering, tracing is a specialized use of logging to record information about a program's execution. This information is typically used by programmers for debugging purposes, and additionally, depending on the type and detail of information contained in a trace log, by experienced system administrators or technical support personnel and software monitoring tools to diagnose common problems with software.

For various reasons, a software development project may not be able to use existing trace systems and be required to develop its own system whatsoever for application trace (via instrumentation of the software) or execution trace (such as trace of instructions executed by a CPU). This is often the case in the embedded world and it was also found in the native software development. In this context, the availability of the trace display component often comes very late in the development cycle of the project, penalizing developers of the upstream parts of the system trace (instrumentation, coding and transport, decoding).

The purpose of the **Trace Viewer Application** (**TVA**) is to make available, to any project, a generic tabular trace viewer, customizable for any type of traces. This allows to give time for the development of a specific project viewer while giving the opportunity to test or verify the various other components of the system trace.
The **Trace Viewer Application** is generic because it is broad enough to allow the visualization of a variety of traces, regardless of their means of capture or encoding.
The **Trace Viewer Application** is  customizable because it provides an easy way to particularize the graphical user interface (title, help, about, names and sizes of columns, etc.), to associate traces decoder processes, and, finally, to activate the application. 
The **Trace Viewer Application** is a host Java application, designed over SWT and Apache Commons libraries,  and running on Linux and Windows. SWT, Standard Widget Toolkit, is an open source widget toolkit for Java designed by Eclipse Consortium to provide efficient, portable access to the user-interface facilities of the operating systems on which it is implemented. The Apache Commons is a project of the Apache Software Foundation;  its purpose of the Commons is to provide reusable, open source ava software.

## LICENSE
**Trace Viewer Application**  is covered by the GNU General Public License (GPL) version 3 and above.

 The notification mechanism is built over Stefan Zeiger (szeiger@novocode.com) work:  shell wrapper which creates balloon popup windows. This work is made available under the terms of the Eclipse Public License v1.0.

## TRACE VIEWER APPLICATION USER INTERFACE
The screen shot below gives an overview of the user interface offered by the Trace viewer Application.

**Users interface for versions 1.x:**

![User interface of TVA](README_images/traceViewer01.png  "User interface of TVA")

**Users interface from versions 2.x:**

![User interface of TVA](README_images/traceViewer02.png  "User interface of TVA")

### Legend
1. Trace area. Trace information is display in columns. Name, width and number of columns are part of the customization. 
2. Bookmark area. The bookmarking allows you to keep a mark on major lines (by double-clicking on them)  in your record and return to them ((by double-clicking on the mark). Marked lines are highlighted.
3. Console for messages issued from TVA or decoders.
4. Progress bar area. The progress bar is a graphical control element used to visualize the progression of  the trace loading.
5. Area of column names (part of the customization).
6. Application name (part of the customization)
7. Two menus are proposed: Source and Help. 
	- Source menu provides access to panels for  acquisition of user's arguments for decoders. It also provides the Exit function to quit the application.
	![Source menu](README_images/traceViewer05.png  "Source menu")
	- Help menu provides help on using the application. Help contents displays the help contents in a help window. About displays information (version, copyright) about the application.
	![Help menur](README_images/traceViewer06.png  "Help menu")

## USING TRACE VIEWER APPLICATION

### Running Trace Viewer Application
Once installed (refer to section "*Installation of Trace Viewer Application*"), the **Trace Viewer Application** can be launched in different ways, in command line  mode or through a file explorer if the host system permits. Whatever the method of launch, we recommend activating a script file rather than directly launch the Java program (refer to section "*Launching Script File*"). In the following examples, tva is the script file delivered in the installation kit for Linux:

``` Bash
	$ traceViewer					# The Application Trace Viewer is installed in a directory referenced by the PATH environment variable.
	
	$ ./traceViewer					# The Application Trace Viewer is installed in the current directory.
	
	$ /home/user/bin/traceViewer	# The Application Trace Viewer is installed in the /home/user/bin directory and it is launched using its absolute path.
```

### Command-Line Arguments
``` Bash
$ traceViewer --help
Usage: traceViewer [OPTION]...
A generic and customizable tabular viewer for traces.

  -h, --help         Print help and exit
  -V, --version      Print version and exit
  -p, --prop=STRING  Basename of the property file to be used.
  -n, --notify       Notify when trace decoding completed.  (default=off)
  -v, --verbose      Verbose mode.  (default=off)
$ 
```

## CUSTOMIZING TRACE VIEWER APPLICATION
To build your application over TVA, you have to:

- provide one or two trace decoders according to the (non-binding) specifications defined in section "*Trace Decoder*".
- associate a property file to your application. This file will contains the configurable parameters of the application. It is through this file that you will customize the graphical user interface. Refer to section "*Property File*" to find a full description of this file.
- Adapt a launching script file to launch your application in a safe way. Refer to section "*Launching Script file*" for details.

### Trace Decoder
A trace decoder is a process parsing either a trace file or a trace flow, and then decoding and formatting these traces before issued them on standard output, in a predefined format.

Path and name of trace decoders (from file and/or from flow) are specified in the property file associated to the final application. Their arguments (predefined or to be defined by the user) are also specified in this file.

The following table gives the list of macro-arguments to be used in the property file to specified user-defined parameters.


| Decoder    | Macro-argument | Explanation |
|---------------|-------------|-------------------------------------------------------|
|               | %f          | Trace file name. To ease the selection via the file browser, a predefined filter is set to the content of the _Suffix variable defined in the property file. Example: --file=%f |
|               | %m          | Additional options to be set by the user. Example: --file=%d -–exec=%e 


Decoded traces are printed on standard output as a string with various fields separated by the character "|". Decoders return a non zero status if an error is detected.

Decoder messages printed on standard error are logged in the Console are of the TVA graphical interface.

Examples of a program that simulate a decoder, written in C, is provided in the TVA package.

### Property File
A property file is a file associated to the user's application built over TVA, with a “*.properties*” file extension and  used to store the configurable parameters of the application.

Each parameter is stored as a pair of strings, one storing the name of the parameter (called the key), and the other storing the value.

Lines that start with '#' are comments.

| Key                   | Content |
|---------------------- |---------|
| _ApplicationName      | Name of the customized TVA. By default: **Trace Viewer Application** |
| _ApplicationVersion   | Version of the customization. By default: **Unknown** |
| _ApplicationCopyright | Text of copyright license applied to the customized TVA. By default, the TVA  copyright license is displayed. |
| _ApplicationHelp      | Text of  on line-help dedicated to the customized TVA.  By default: **no help** |
| _ColumsName           | Names of columns to display. The value consists of strings separated by a comma. A first column is always implicitly added to display the trace line number. By default: **MasterID, Channel, Sequence, Level, Information** |
| _ColumsWidth          | Width of columns defined in _ColumsName variable. The value consists of decimal number separated by a comma By default: **100, 70, 80, 100, 300** |
| _FileDecoderExe       | Name with absolute path of the trace decoder. |
| _FileDecoderArg       | Arguments for the trace decoder. |
| _Suffix               | Predefined filter to ease the selection if the file browser. By default: no filter, that means that the implicit filter \*.\* will be used. |



Example:
``` Bash
$ cat MyATL.property
#Thu Oct 16 16:20:09 CEST 2014
_ApplicationName=My\ Application\ Trace\ Logger
_ApplicationVersion=1.0.0
_ApplicationCopyright=Copyright (c) 2014, My Company\nAll rights reserved.
_ApplicationHelp=Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
_ColumsName=MasterID, Channel, Sequence, Level, Information
_ColumsWidth=100, 70, 80, 100, 300
_Suffix=trc
_FileDecoderExe=/home/user/bin/decoderC
_FileDecoderArg=--file\=%f
$
```

### STRUCTURE OF THE APPLICATION
This section walks you through **Trace Viewer Application**'s structure. Once you understand this structure, you will easily find your way around in **Trace Viewer Application**'s code base.
``` Bash
$ yaTree
./                                                                     # Application level
├── README_images/                                                     # Images for documentation
│   ├── traceViewer01.png                                              # 
│   ├── traceViewer02.png                                              # 
│   ├── traceViewer05.png                                              # 
│   └── traceViewer06.png                                              # 
├── tests/                                                             # Test directory
│   ├── ABC.properties                                                 # Property file for test
│   ├── Makefile                                                       # Makefile
│   └── myDecoder.c                                                    # Pseudo-decoder for test
├── traceViewer_bin/                                                   # Binary directory: jars (third-parties and local) and driver
│   ├── Makefile                                                       # Makefile
│   ├── commons-cli-1.4.jar                                            # Third-party COMMONS CLI jar file
│   ├── commons-exec-1.3.jar                                           # Third-party COMMONS EXEC jar file
│   ├── org.eclipse.nebula.widgets.led_1.0.0.201912241810.jar          # Third-party ECLIPSE jar file
│   ├── org.eclipse.nebula.widgets.opal.commons_1.0.0.201912241810.jar # Third-party ECLIPSE jar file
│   └── swt-linux-415.jar                                              # Third-party SWT jar file
├── traceViewer_c/                                                     # C Source directory
│   ├── Makefile                                                       # Makefile
│   ├── traceViewer.c                                                  # C main source file (application driver)
│   └── traceViewer.ggo                                                # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── traceViewer_java/                                                  # JAVA Source directory
│   ├── traceViewer/                                                   # IntelliJ registerView project structure
│   │   ├── src/                                                       # 
│   │   │   ├── balloonWindow.java                                     # 
│   │   │   ├── traceViewer.java                                       # 
│   │   │   ├── traceViewerBuildInfo.java                              # 
│   │   │   ├── traceViewerDecoder.java                                # 
│   │   │   ├── traceViewerGui.java                                    # 
│   │   │   ├── traceViewerHelp.java                                   # 
│   │   │   └── traceViewerNumberOfLine.java                           # 
│   │   └── traceViewer.iml                                            # 
│   └── Makefile                                                       # Makefile
├── COPYING.md                                                         # GNU General Public License markdown file
├── LICENSE.md                                                         # License markdown file
├── Makefile                                                           # Makefile
├── README.md                                                          # ReadMe markdown file
├── RELEASENOTES.md                                                    # Release Notes markdown file
└── VERSION                                                            # Version identification text file

7 directories, 31 files
```

## HOW TO BUILD THIS APPLICATION
``` Bash
$ cd traceViewer
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
``` Bash
$ cd traceViewer
$ make release
	# Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
	# We consider that $BIN_DIR is a part of the PATH.
```

## HOW TO RUN THE APPLICATION
``` Bash
$ cd traceViewer
$ make release
$ cd tests
$ # .... Tune ABC.properties file
$ cp ABC.properties $BIN_DIR/traceViewer_bin
$ gcc -DLINUX ABC.out ABC.c
$ ./ABC.out > ABC.bin
$ traceViewer --prop=ABC --notify --verbose
	# Select "Source -> Frm file..." and choose ABC.bin
	# Select "Source -> Direct connection..." and click "OK" on the next panel
```

## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *traceViewer/traceViewer_java/traceViewer* project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
	- JAVA 1.8.0 for usage and development
- For development:
  - IntelliJ IDEA 2019.2.4 (Community Edition) Build #IC-192.7142.36, built on October 29, 2019
  - Openjdk:
    - version "1.8.0_232-ea"
    - OpenJDK Runtime Environment (build 1.8.0_232-ea-8u232-b09-0ubuntu1-b09)
    - OpenJDK 64-Bit Server VM (build 25.232-b09, mixed mode)
  - GNU gengetopt 2.23

Application developed and tested with XUBUNTU 20.04

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES) .

***