//------------------------------------------------------------------------------
// Copyright (c) 2014-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

class traceViewerHelp {
	private static final String sTitleHelp	= traceViewer.applicationName + " - Help";
	private static final String sHelpMessage	= "Some generic and helpful documentation to be placed here......"
											+ traceViewer.eol + traceViewer.eol;
	private static final String sTitleAbout	= traceViewer.applicationName + " - About";

	static void HelpView() {
		Shell shell = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setText(sTitleHelp);
		shell.setLayout(new FillLayout());
		shell.setSize(700, 200);
		shell.setBackground(traceViewer.ColorShell);
		shell.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		Text help = new Text(shell, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
		help.setLayoutData(new GridData(GridData.FILL_BOTH));
		help.setBackground(traceViewer.ColorShell);
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		help.setText(traceViewer.applicationHelp.length() == 0 ? sHelpMessage
				: traceViewer.applicationHelp + traceViewer.eol + traceViewer.eol + sHelpMessage);
		help.setSelection(0);

		// Center the shell on the primary monitor
		Monitor primary = Display.getCurrent().getPrimaryMonitor ();
		Rectangle bounds = primary.getBounds ();
		Rectangle rect = shell.getBounds ();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation (x, y);

		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}

	static void AboutView() {
		final Shell shell = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setText(sTitleAbout);
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setBackground(traceViewer.ColorShell);
		shell.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		shell.setLayout(new FillLayout());
		shell.setSize(500, 300);

		StyledText about = new StyledText(shell, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
		about.setLayoutData(new GridData(GridData.FILL_BOTH));
		about.setBackground(traceViewer.ColorShell);
		about.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		about.setFont(traceViewer.fontText);
		String s = traceViewer.eol
				+ traceViewer.applicationName + traceViewer.eol + "\t\tVersion: " + traceViewer.applicationVers
				+ traceViewer.eol + traceViewer.eol
				+ "built on "
				+ traceViewer.eol + traceViewer.eol
				+ "\t" + traceViewer.sTitle + " (TVA)\n\t\tBuild: " + traceViewerBuildInfo.getNumber()
				+ " - Date: " + traceViewerBuildInfo.getDate()
				+ traceViewer.eol + traceViewer.eol;
		StyleRange style1 = new StyleRange();
		style1.start = 0;
		style1.length = s.length();
		style1.fontStyle = SWT.BOLD;
		s += traceViewer.applicationCopy;
		about.setText(s);
		about.setStyleRange(style1);
		about.setSelection(0);

		// Center the shell on the primary monitor
		Monitor primary = Display.getCurrent().getPrimaryMonitor ();
		Rectangle bounds = primary.getBounds ();
		Rectangle rect = shell.getBounds ();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		shell.setLocation (x, y);

		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
}
//==============================================================================
