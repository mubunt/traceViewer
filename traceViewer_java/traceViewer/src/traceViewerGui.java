//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.nebula.widgets.led.LED;
import org.eclipse.nebula.widgets.led.LEDCharacter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

class traceViewerGui {
	static Shell SHELL;
	static Table VIEWER;
	private static Canvas MARKER;
	private static Text CONSOLE;
	private static Cursor cursor;
	private static LED counter1, counter2, counter3, counter4, counter5, counter6;

	private static String CurrentDir = "";
	private static String CurrentFile = "";
	static String DataFile = "";
	static String MoreOptions = "";
	private static final int[] markers = new int[traceViewer.maxMarker];

	private static Text tDataFile;
	private static Text tMoreOptions;

	private static final int console_height	= 100;
	private static final int console_width = 400;
	private static final int viewer_height = 600;
	//======================================================================
	static void TraceViewer() {
		//----------------------------------------------------------------------
		// UI INITIALIZATIONS
		cursor = Display.getCurrent().getSystemCursor(SWT.CURSOR_ARROW);
		//----------------------------------------------------------------------
		// SHELL
		SHELL = new Shell(Display.getCurrent(), SWT.MAX | SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER | SWT.RESIZE);
		SHELL.setText(traceViewer.applicationName);
		SHELL.setCursor(cursor);
		SHELL.setBackground(traceViewer.ColorShell);
		SHELL.setLayout(new GridLayout(1, true));
		SHELL.addListener(SWT.Close, (Event e) -> { e.doit = false; traceViewer.Exit(0); });
		//----------------------------------------------------------------------
		// USER INTERFACE BUILDING
		traceViewerNumberOfLine.init();
		initUI();
		//----------------------------------------------------------------------
		// USER INTERFACE DISPLAY
		SHELL.pack();
		SHELL.setSize(900, 700);
		// Center the shell on the primary monitor
		Monitor primary = Display.getCurrent().getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = SHELL.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		SHELL.setLocation(x, y);
		//----------------------------------------------------------------------
		// GO ON
		CurrentDir = traceViewer.homedir;
		SHELL.open();
		while (!SHELL.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
	//--------------------------------------------------------------------------
	private static void initUI() {
		// TOOLBAR
		Menu menuBar = new Menu(SHELL, SWT.BAR);
		SHELL.setMenuBar(menuBar);
		final MenuItem decodeItem = new MenuItem(menuBar, SWT.PUSH);
		final MenuItem helpItem = new MenuItem(menuBar, SWT.PUSH);
		final MenuItem aboutItem = new MenuItem(menuBar, SWT.PUSH);
		// Decoding
		decodeItem.setText("&Decoding");
		decodeItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) { traceViewerDecoder.runTraceDecoderExe(); }
		});
		// Help
		helpItem.setText("&Help");
		helpItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) { traceViewerHelp.HelpView(); }
		});
		// About
		aboutItem.setText("&About");
		aboutItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) { traceViewerHelp.AboutView(); }
		});
		// TRACE DISPLAY AREA
		Composite compositeT = new Composite(SHELL, SWT.FILL);

		GridData gd0 = new GridData();
		gd0.grabExcessVerticalSpace = gd0.grabExcessHorizontalSpace = true;
		gd0.horizontalAlignment = GridData.FILL;
		gd0.horizontalIndent = 0;
		gd0.verticalAlignment = GridData.FILL;
		gd0.verticalIndent = 0;

		compositeT.setLayoutData(gd0);
		compositeT.setLayout(new GridLayout(1, false));
		compositeT.setBackground(traceViewer.ColorShell);

		final Composite outerGroup0 = new Composite(compositeT, SWT.FILL);
		outerGroup0.setLayout(new GridLayout(2, false));
		outerGroup0.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		// Markers
		MARKER = new Canvas(outerGroup0, SWT.BORDER);

		final GridData gd5 = new GridData();
		gd5.grabExcessHorizontalSpace = false;
		gd5.grabExcessVerticalSpace = true;
		gd5.heightHint = viewer_height;
		gd5.widthHint = gd5.minimumWidth = traceViewer.markerWidth;
		gd5.horizontalAlignment = GridData.FILL;
		gd5.horizontalIndent = 0;
		gd5.verticalAlignment = GridData.FILL;
		gd5.verticalIndent = 0;

		MARKER.setLayoutData(gd5);
		MARKER.setBackground(traceViewer.ColorShell);
		MARKER.addPaintListener(e -> {
            e.gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_GRAY));
            e.gc.fillRectangle(2, 2, traceViewer.markerWidth - 4, traceViewer.markerHeaderHeight - 2);
            e.gc.dispose();
            drawMarkers(traceViewerNumberOfLine.get());
        });
		InitMarker();
		// Trace Viewer
		VIEWER = new Table(outerGroup0, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION);

		GridData gd6 = new GridData();
		gd6.grabExcessVerticalSpace = gd6.grabExcessHorizontalSpace = true;
		gd6.heightHint = viewer_height;
		gd6.widthHint = gd6.minimumWidth = 10;
		gd6.horizontalAlignment = GridData.FILL;
		gd6.horizontalIndent = 0;
		gd6.horizontalSpan = 1;
		gd6.verticalAlignment = GridData.FILL;
		gd6.verticalSpan = gd6.verticalIndent = 0;

		VIEWER.setLayoutData(gd6);
		VIEWER.setLinesVisible(true);
		VIEWER.setHeaderVisible(true);
		VIEWER.setBackground(traceViewer.ColorShell);
		VIEWER.setForeground(traceViewer.ColorTrace);

		for (int e = 0; e < traceViewer.columnNames.length; ++e)
			(new TableColumn(VIEWER, SWT.NONE)).setText(traceViewer.columnNames[e]);
		for (int e = 0; e < traceViewer.columnNames.length; ++e)
			VIEWER.getColumn(e).pack();
		for (int e = 0; e < traceViewer.columnWidths.length; ++e)
			VIEWER.getColumn(e).setWidth(traceViewer.columnWidths[e]);

		VIEWER.addListener(SWT.MouseDoubleClick, e -> {
            if (traceViewerNumberOfLine.get() == 0)
                return;
            Rectangle clientArea = VIEWER.getClientArea();
            Point pt = new Point(e.x, e.y);
            for (int index = VIEWER.getTopIndex(); index < VIEWER.getItemCount(); ++index) {
                boolean visible = false;
                TableItem item = VIEWER.getItem(index);
                for (int i = 0; i < traceViewer.columnNames.length; ++i) {
                    Rectangle rect = item.getBounds(i);
                    if (rect.contains(pt))
                        ProcessMarker(index);
                    if (!visible && rect.intersects(clientArea))
                        visible = true;
                }
                if (!visible)
                    return;
            }
        });
		MARKER.addListener(SWT.MouseDoubleClick, e -> {
            int l;
            if (traceViewerNumberOfLine.get() != 0 && (l = findMarker(e.y, traceViewerNumberOfLine.get())) != -1)
                VIEWER.setSelection(l);
        });
		// TRACE CONSOLE AND COUNTER or PROGRESS BAR
		final Composite outerGroup1 = new Composite(compositeT, SWT.FILL);
		outerGroup1.setLayout(new GridLayout(2, false));
		outerGroup1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		createConsole(outerGroup1);
		createCounter(outerGroup1);
		writeCounter(traceViewerNumberOfLine.get());
	}
	//======================================================================
	// MARKER MANAGEMENT
	//----------------------------------------------------------------------
	private static void InitMarker() {
		for (int e = 0; e < traceViewer.maxMarker; ++e)
			markers[e] = -1;
	}
	//----------------------------------------------------------------------
	private static void ProcessMarker(int mark) {
		int firstFreeLocation = -1;
		boolean found = false;

		for (int e = 0; e < traceViewer.maxMarker; ++e) {
			if (markers[e] == mark) {
				markers[e] = -1;
				found = true;
				break;
			}
			if (markers[e] == -1 && firstFreeLocation == -1)
				firstFreeLocation = e;
		}
		if (found) {
			if (traceViewer.verboseMode)
				writeConsole("Removed marker on line " + mark + traceViewer.eol);
			highlightLine(mark, false);
			drawMarkers(traceViewerNumberOfLine.get());
		} else if (firstFreeLocation == -1)
			writeWarning("The maximum number of markers (" + traceViewer.maxMarker + ") is achieved!");
		else {
			markers[firstFreeLocation] = mark;
			if (traceViewer.verboseMode)
				writeConsole("Recorded marker on line " + mark + traceViewer.eol);
			highlightLine(mark, true);
			drawMarkers(traceViewerNumberOfLine.get());
		}
	}
	//----------------------------------------------------------------------
	static void drawMarkers(final int nbl) {
		if (!Display.getCurrent().isDisposed())
			Display.getCurrent().syncExec(() -> {
                if (VIEWER.isDisposed() || nbl == 0)
                    return;
                int height = MARKER.getBounds().height - traceViewer.markerHeaderHeight - 1;
                int h = height / nbl == 0 ? 1 : height / nbl;
                GC gc = new GC(MARKER);
				gc.setBackground(traceViewer.ColorShell);
				gc.fillRectangle(0, traceViewer.markerHeaderHeight, traceViewer.markerWidth, height);
                gc.dispose();
                for (int i = 0; i < traceViewer.maxMarker; ++i)
                    if (markers[i] != -1) {
                        gc = new GC(MARKER);
                        gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
                        int y = traceViewer.markerHeaderHeight + (int) ((height - h) * markers[i] / (float) nbl)
                                - 1;
                        gc.drawRectangle(2, y, traceViewer.markerWidth - 4, h);
                        gc.dispose();
                    }
            });
	}
	//----------------------------------------------------------------------
	private static int findMarker(final int y, final int nbl) {
		if (nbl == 0) return(-1);
		int height = MARKER.getBounds().height - traceViewer.markerHeaderHeight;	// Available height of the marker bar at the time of this call....
		int h = height / nbl == 0 ? 1 : height / nbl;	// height of each marker
		int index = (int) ((float) nbl * (y - traceViewer.markerHeaderHeight + 1)) / (height - h);
		int delta = nbl + 1;
		int closestindex = nbl + 1;

		for (int i = 0; i < traceViewer.maxMarker; ++i) {
			if (markers[i] == index) return(index);
			if (markers[i] != -1) {
				int tmp = Math.min(Math.abs(closestindex - index), Math.abs(markers[i] - index));
				if (delta > tmp) { delta = tmp; closestindex = markers[i]; }
			}
		}
		return(closestindex);
	}
	//----------------------------------------------------------------------
	private static void highlightLine(int mark, boolean on) {
		VIEWER.getItems()[mark].setBackground(on ? traceViewer.ColorMarkedLine : traceViewer.ColorShell);
		VIEWER.getItems()[mark].setForeground(on ? traceViewer.ColorMarkedTrace : traceViewer.ColorTrace);
	}
	//======================================================================
	// FILE SELECTION
	//----------------------------------------------------------------------
	private static String traceFileChooser() {
		FileDialog dialog = new FileDialog(SHELL, SWT.OPEN);
		dialog.setFilterNames(new String[] { "Trace Files (*." + traceViewer.traceFileSuffix + ")", "All Files (*.*)" });
		dialog.setFilterExtensions(new String[] { "*." + traceViewer.traceFileSuffix, "*.*" });
		dialog.setFilterPath(CurrentDir);
		dialog.setFileName(CurrentFile);
		String selected = dialog.open();
		CurrentDir = dialog.getFilterPath();
		if (selected != null) selected = selected.replace("\\", "\\\\");
		return selected;
	}
	//======================================================================
	// OPTIONS for CONNECTION
	//----------------------------------------------------------------------
	static boolean traceDecoderOption() {
		final boolean[] result = new boolean[1];

		final Shell dialog = new Shell(SHELL, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setCursor(cursor);
		dialog.setBackground(traceViewer.ColorShell);
		dialog.setLayout(new GridLayout(1, false));
		
		Group outerGroup0 = new Group(dialog, SWT.BORDER);
		outerGroup0.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		outerGroup0.setLayout(new GridLayout(3, false));
		outerGroup0.setText("Option(s) for Trace Decoder");
		outerGroup0.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		if (traceViewer.traceFiledecoderarg.contains("%f")) {
			Label lDataFile = new Label(outerGroup0, SWT.NONE);
			lDataFile.setText("&Trace File: ");
			lDataFile.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));

			tDataFile = new Text(outerGroup0, SWT.BORDER | SWT.SINGLE);
			tDataFile.setBackground(traceViewer.ColorShell);
			tDataFile.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
			tDataFile.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			if (DataFile.length() != 0) tDataFile.setText(DataFile);
			tDataFile.addListener(SWT.Verify, __ -> { });

			Button bDataFile = new Button(outerGroup0, SWT.PUSH);
			bDataFile.setText("Browse ...");
			bDataFile.setBackground(traceViewer.ColorShell);
			bDataFile.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
			bDataFile.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
			bDataFile.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent __) {
					CurrentFile = traceFileChooser();
					if (CurrentFile == null)
						return;
					DataFile = CurrentFile;
					tDataFile.setText(DataFile);
				}
			});
		}

		if (traceViewer.traceFiledecoderarg.contains("%m")) {
			Label lMoreOptions = new Label(outerGroup0, SWT.NULL);
			lMoreOptions.setText("&Other Options: ");
			lMoreOptions.setBackground(traceViewer.ColorShell);
			lMoreOptions.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
			lMoreOptions.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, true));

			tMoreOptions = new Text(outerGroup0, SWT.BORDER | SWT.SINGLE);
			tMoreOptions.setBackground(traceViewer.ColorShell);
			tMoreOptions.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
			tMoreOptions.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
			if (MoreOptions.length() != 0) tMoreOptions.setText(MoreOptions);
			tMoreOptions.addModifyListener(__ -> { });
		}

		Label label = new Label(dialog, SWT.NULL);
		label.setText(" ");
		label.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, true));

		Composite c1 = new Composite(dialog, SWT.NONE);
		c1.setBackground(traceViewer.ColorShell);
		c1.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		FormLayout f1 = new FormLayout();
		c1.setLayout(f1);

		Button cancelButton = new Button (c1, SWT.PUSH);
		cancelButton.setText("&Cancel");
		cancelButton.setBackground(traceViewer.ColorShell);
		cancelButton.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		final Button okButton = new Button (c1, SWT.PUSH);
		okButton.setText("&OK");
		okButton.setBackground(traceViewer.ColorShell);
		okButton.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		FormData cancelData = new FormData();
		cancelData.right = new FormAttachment(100, -5);
		cancelData.bottom = new FormAttachment(95);
		cancelData.top = new FormAttachment(0, 30);
		cancelButton.setLayoutData(cancelData);

		FormData okData = new FormData();
		okData.right = new FormAttachment(cancelButton, -5, SWT.LEFT);
		okData.bottom = new FormAttachment(cancelButton, 0, SWT.BOTTOM);
		okButton.setLayoutData(okData);

		Listener listener = (Event e) -> {
            result[0] = e.widget == okButton;
            if (result[0]) {
                if (traceViewer.traceFiledecoderarg.contains("%f"))
                    DataFile = tDataFile.getText();
                if (traceViewer.traceFiledecoderarg.contains("%m"))
                    MoreOptions = tMoreOptions.getText();
            }
			dialog.close();
        };

		okButton.addListener(SWT.Selection, listener);
		cancelButton.addListener(SWT.Selection, listener);

		dialog.setDefaultButton(okButton);
		dialog.pack();
		dialog.setSize(550, 250);
		dialog.open();

		while (!dialog.isDisposed ())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
		return result[0];
	}
	//======================================================================
	// VARIOUS ROUTINES
	//----------------------------------------------------------------------
	private static void writeWarning(final String s) {
		MessageBox mb = new MessageBox(new Shell(), SWT.OK | SWT.ICON_WARNING);
		mb.setText("Warning");
		mb.setMessage(s);
		mb.open();
	}
	//----------------------------------------------------------------------
	private static void createConsole(Composite parent) {
		CONSOLE = new Text(parent, SWT.BORDER | SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL | SWT.WRAP);
		CONSOLE.setBackground(traceViewer.consoleBackground);
		CONSOLE.setForeground(traceViewer.consoleForeground);
		CONSOLE.setFont(traceViewer.fontConsole);
		final GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.heightHint = console_height;
		gd.widthHint = console_width;
		CONSOLE.setLayoutData(gd);
	}

	static void writeConsole(final String e) {
		Display.getDefault().asyncExec(() -> {
            CONSOLE.setSelection(CONSOLE.getText().length());
            CONSOLE.insert(e);
        });
	}
	//----------------------------------------------------------------------
	private static void createCounter(Composite parent) {
		Composite counter = new Composite(parent, SWT.NONE);
		counter.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));
		counter.setLayout(new GridLayout(6, false));
		counter.setBackground(traceViewer.ColorShell);

		counter1 = new LED(counter, SWT.NONE);
		counter1.setSelectedColor(traceViewer.ColorLed);
		counter1.setLayoutData(new GridData(GridData.END, GridData.FILL, true, false));
		counter2 = new LED(counter, SWT.NONE);
		counter2.setSelectedColor(traceViewer.ColorLed);
		counter2.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		counter3 = new LED(counter, SWT.NONE);
		counter3.setSelectedColor(traceViewer.ColorLed);
		counter3.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		counter4 = new LED(counter, SWT.NONE);
		counter4.setSelectedColor(traceViewer.ColorLed);
		counter4.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		counter5 = new LED(counter, SWT.NONE);
		counter5.setSelectedColor(traceViewer.ColorLed);
		counter5.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
		counter6 = new LED(counter, SWT.NONE);
		counter6.setSelectedColor(traceViewer.ColorLed);
		counter6.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, false));
	}

	static void writeCounter(final int c) {
		//Display.getDefault().syncExec(() -> COUNTER.setText(String.format("%06d", c)));
		Display.getDefault().syncExec(() -> {
			counter1.setCharacter(LEDCharacter.getByNumber(getMajorDigit(c, 5)));
			counter2.setCharacter(LEDCharacter.getByNumber(getMajorDigit(c, 4)));
			counter3.setCharacter(LEDCharacter.getByNumber(getMajorDigit(c, 3)));
			counter4.setCharacter(LEDCharacter.getByNumber(getMajorDigit(c, 2)));
			counter5.setCharacter(LEDCharacter.getByNumber(getMajorDigit(c, 1)));
			counter6.setCharacter(LEDCharacter.getByNumber(c % 10));
		});
	}
	private static int getMajorDigit(int value, int rank) {
		int val = value % (int) Math.pow(10, rank + 1);
		return (val - (val % ((int) Math.pow(10, rank)))) / (int) Math.pow(10, rank);
	}
}
//==============================================================================