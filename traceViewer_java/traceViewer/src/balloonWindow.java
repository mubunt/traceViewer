/*******************************************************************************
 * Copyright (c) 2004 Stefan Zeiger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.novocode.com/legal/epl-v10.html
 *
 * Contributors:
 *     Stefan Zeiger (szeiger@novocode.com) - initial API and implementation
 *******************************************************************************/
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

import java.util.ArrayList;

/**
 * A Shell wrapper which creates balloon popup windows.
 * 
 * <p>By default, a balloon window has no title bar or system controls.
 * The following styles are supported:</p>
 * 
 * <ul>
 *   <li>SWT.ON_TOP - Keep the window on top of other windows</li>
 *   <li>SWT.TOOL - Add a drop shadow to the window (on supported platforms)</li>
 *   <li>SWT.CLOSE - Show a "close" control on the title bar (implies SWT.TITLE)</li>
 *   <li>SWT.TITLE - Show a title bar</li>
 * </ul>
 * 
 * @author Stefan Zeiger (szeiger@novocode.com)
 * @since Jul 2, 2004
 * @version $Id: BalloonWindow.java 340 2005-06-25 19:01:30 +0000 (Sat, 25 Jun 2005) szeiger $
 */

@SuppressWarnings({"MismatchedQueryAndUpdateOfCollection", "FieldCanBeLocal", "unchecked"})
class balloonWindow {
	private final Shell shell;
	private final Composite contents;
	private Label titleLabel;
	private Canvas titleImageLabel;
	private final int style;
	private final int preferredAnchor = SWT.RIGHT | SWT.BOTTOM;
	private final boolean autoAnchor = true;
	private int locX = Integer.MIN_VALUE, locY = Integer.MIN_VALUE;
	private final int marginLeft = 12, marginRight = 12, marginTop = 5, marginBottom = 10;
	private final int titleSpacing = 3, titleWidgetSpacing = 8;
	private ToolBar systemControlsBar;
	private final ArrayList selectionControls = new ArrayList();
	private boolean addedGlobalListener;
	private final ArrayList selectionListeners = new ArrayList();

	balloonWindow(Shell parent, int style) {
		this(null, parent, style);
	}

	// balloonWindow(Display display, int style) {
	//	this(display, null, style);
	// }
	
	private balloonWindow(Display display, Shell parent, final int style) {
		this.style = style;
		int shellStyle = style & (SWT.TOOL | SWT.ON_TOP);
		this.shell = (display != null)
			? new Shell(display, shellStyle | SWT.NO_TRIM)
			: new Shell(parent, shellStyle | SWT.NO_TRIM);
		this.contents = new Composite(shell, SWT.NONE);
		
		final Color c = new Color(shell.getDisplay(), 255, 255, 225);
		shell.setBackground(c);
		shell.setForeground(shell.getDisplay().getSystemColor(SWT.COLOR_BLACK));
		contents.setBackground(shell.getBackground());
		contents.setForeground(shell.getForeground());

		selectionControls.add(shell);
		selectionControls.add(contents);

		final Listener globalListener = e -> {
            for (int i = selectionControls.size() - 1; i >= 0; i--)
                if (selectionControls.get(i) == e.widget) {
                    if ((style & SWT.CLOSE) == 0)
                        shell.close();
                    else
                        for (int j = selectionListeners.size() - 1; j >= 0; j--)
                            ((Listener) selectionListeners.get(j)).handleEvent(e);
                    e.doit = false;
                }
        };

		shell.addListener(SWT.Show, __ -> {
            if (addedGlobalListener)
                return;
            shell.getDisplay().addFilter(SWT.MouseDown, globalListener);
            addedGlobalListener = true;
        });

		shell.addListener(SWT.Hide, __ -> {
            if (!addedGlobalListener)
                return;
            shell.getDisplay().removeFilter(SWT.MouseDown, globalListener);
            addedGlobalListener = false;
        });

		shell.addListener(SWT.Dispose, __ -> {
            if (addedGlobalListener) {
                shell.getDisplay().removeFilter(SWT.MouseDown, globalListener);
                addedGlobalListener = false;
            }
            c.dispose();
        });
	}

	/*
	  Adds a control to the list of controls which close the balloon window.
	  The background, title image and title text are included by default.
	 */

	// void addSelectionControl(Control ¢) {
	//	selectionControls.add(¢);
	// }

	// void addListener(int type, Listener l) {
	//	if (type == SWT.Selection) selectionListeners.add(l);
	// }

	/**
	 * Set the location of the anchor. This must be one of the following values:
	 * SWT.NONE, SWT.LEFT|SWT.TOP, SWT.RIGHT|SWT.TOP, SWT.LEFT|SWT.BOTTOM, SWT.RIGHT|SWT.BOTTOM
	 */
	// void setAnchor(int anchor) {
	//	switch(anchor) {
	//		case SWT.NONE:
	//		case SWT.TOP | SWT.LEFT:
	//		case SWT.TOP | SWT.RIGHT:
	//		case SWT.LEFT|SWT.BOTTOM:
	//		case SWT.RIGHT|SWT.BOTTOM:
	//			break;
	//		default:
	//			throw new IllegalArgumentException("Illegal anchor value "+anchor);
	//	}
	//	this.preferredAnchor = anchor;
	// }

	// void setAutoAnchor(boolean autoAnchor) {
	//	this.autoAnchor = autoAnchor;
	// }

	void setLocation(int x, int y) {
		this.locX = x;
		this.locY = y;
	}

	// void setLocation(Point ¢) {
	//	this.locX = ¢.x;
	//	this.locY = ¢.y;
	// }

	void setText(String title) {
		shell.setText(title);
	}

	// void setFont(Font ¢) {
	//	shell.setFont(¢);
	// }

	// void setImage(Image ¢) {
	//	shell.setImage(¢);
	// }

	// void setMargins(int marginLeft, int marginRight, int marginTop, int marginBottom) {
	//	this.marginLeft = marginLeft;
	//	this.marginRight = marginRight;
	//	this.marginTop = marginTop;
	//	this.marginBottom = marginBottom;
	// }

	// void setMargins(int marginX, int marginY) {
	//	setMargins(marginX, marginX, marginY, marginY);
	// }

	// void setMargins(int margin) {
	//	setMargins(margin, margin, margin, margin);
	// }

	// void setTitleSpacing(int titleSpacing) {
	//	this.titleSpacing = titleSpacing;
	//}

	// void setTitleWidgetSpacing(int titleImageSpacing) {
	//	this.titleWidgetSpacing = titleImageSpacing;
	// }

	Shell getShell() {
		return shell;
	}

	Composite getContents() {
		return contents;
	}

	private void prepareForOpen() {
		Point contentsSize = contents.getSize();
		Point titleSize = new Point(0, 0);

		boolean showTitle = ((style & (SWT.CLOSE | SWT.TITLE)) != 0);
		if (showTitle) {
			if (titleLabel == null) {
				titleLabel = new Label(shell, SWT.NONE);
				titleLabel.setBackground(shell.getBackground());
				titleLabel.setForeground(shell.getForeground());
				FontData[] fds = shell.getFont().getFontData();
				for (int ¢=0; ¢<fds.length; ++¢)
					fds[¢].setStyle(SWT.BOLD | fds[¢].getStyle());
				final Font font = new Font(shell.getDisplay(), fds);
				titleLabel.addListener(SWT.Dispose, __ -> font.dispose());
				titleLabel.setFont(font);
				selectionControls.add(titleLabel);
			}
			String titleText = shell.getText();
			titleLabel.setText(titleText == null ? "" : titleText);
			titleLabel.pack();
			titleSize = titleLabel.getSize();

			final Image titleImage = shell.getImage();
			if (titleImageLabel == null && shell.getImage() != null) {
				titleImageLabel = new Canvas(shell, SWT.NONE);
				titleImageLabel.setBackground(shell.getBackground());
				titleImageLabel.setBounds(titleImage.getBounds());
				titleImageLabel.addListener(SWT.Paint, ¢ -> ¢.gc.drawImage(titleImage, 0, 0));
				Point tilSize = titleImageLabel.getSize();
				titleSize.x += tilSize.x + titleWidgetSpacing;
				if (tilSize.y > titleSize.y) titleSize.y = tilSize.y;
				selectionControls.add(titleImageLabel);
			}

			if (systemControlsBar == null && (style & SWT.CLOSE) != 0) {
				//Color closeFG = shell.getForeground(), closeBG = shell.getBackground();
				//Color closeFG = shell.getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY), closeBG = shell.getBackground();
				Color closeFG = shell.getDisplay().getSystemColor(SWT.COLOR_WIDGET_FOREGROUND), closeBG = shell.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
				final Image closeImage = createCloseImage(shell.getDisplay(), closeBG, closeFG);
				shell.addListener(SWT.Dispose, __ -> closeImage.dispose());
				systemControlsBar = new ToolBar(shell, SWT.FLAT);
				systemControlsBar.setBackground(closeBG);
				systemControlsBar.setForeground(closeFG);
				ToolItem closeItem = new ToolItem(systemControlsBar, SWT.PUSH);
				closeItem.setImage(closeImage);
				closeItem.addListener(SWT.Selection, __ -> shell.close());
				systemControlsBar.pack();
				Point closeSize = systemControlsBar.getSize();
				titleSize.x += closeSize.x + titleWidgetSpacing;
				if (closeSize.y > titleSize.y)
					titleSize.y = closeSize.y;
			}

			titleSize.y += titleSpacing;
			if (titleSize.x > contentsSize.x) {
				contentsSize.x = titleSize.x;
				contents.setSize(contentsSize.x, contentsSize.y);
			}
			contentsSize.y += titleSize.y;
		}

		Rectangle screen = shell.getDisplay().getClientArea();

		int anchor = preferredAnchor;
		//noinspection ConstantConditions
		if (anchor != SWT.NONE && autoAnchor && locX != Integer.MIN_VALUE) {
			if ((anchor & SWT.LEFT) == 0) {
				if (locX - marginLeft - marginRight - contentsSize.x + 16 < screen.x)
					anchor = anchor - SWT.RIGHT + SWT.LEFT;
			} else if (locX + marginLeft + marginRight + contentsSize.x - 16 >= screen.x + screen.width)
				anchor = anchor - SWT.LEFT + SWT.RIGHT;
			if ((anchor & SWT.TOP) == 0) {
				if (locY - marginTop - marginBottom - contentsSize.y - 20 < screen.y)
					anchor = anchor - SWT.BOTTOM + SWT.TOP;
			} else if (locY + marginTop + marginBottom + contentsSize.y + 20 >= screen.y + screen.height)
				anchor = anchor - SWT.TOP + SWT.BOTTOM;
		}

		final Point shellSize = new Point(contentsSize.x + marginLeft + marginRight, anchor == SWT.NONE
				? contentsSize.y + marginTop + marginBottom : contentsSize.y + marginTop + marginBottom + 20);

		if (shellSize.x < marginLeft + marginRight + 54)
			shellSize.x = 54 + marginLeft + marginRight;
		if (anchor == SWT.NONE) {
			if (shellSize.y < marginTop + marginBottom + 10)
				shellSize.y = 10 + marginTop + marginBottom;
		}
		else {
			if (shellSize.y < marginTop + marginBottom + 30)
				shellSize.y = 30 + marginTop + marginBottom;
		}

		shell.setSize(shellSize);
		int titleLocY = marginTop + ((anchor & SWT.TOP) == 0 ? 0 : 20);
		contents.setLocation(marginLeft, titleSize.y + titleLocY);
		if (showTitle) {
			int realTitleHeight = titleSize.y - titleSpacing;
			if (titleImageLabel == null)
				titleLabel.setLocation(marginLeft, titleLocY + (realTitleHeight - titleLabel.getSize().y) / 2);
			else {
				titleImageLabel.setLocation(marginLeft,
						titleLocY + (realTitleHeight - titleImageLabel.getSize().y) / 2);
				titleLabel.setLocation(marginLeft + titleImageLabel.getSize().x + titleWidgetSpacing,
						titleLocY + (realTitleHeight - titleLabel.getSize().y) / 2);
			}
			if (systemControlsBar != null)
				systemControlsBar.setLocation(shellSize.x - marginRight - systemControlsBar.getSize().x, titleLocY + (realTitleHeight-systemControlsBar.getSize().y)/2);
		}

		final Region region = new Region();
		region.add(createOutline(shellSize, anchor, true));

		shell.setRegion(region);
		shell.addListener(SWT.Dispose, __ -> region.dispose());

		final int[] outline = createOutline(shellSize, anchor, false);
		shell.addListener(SWT.Paint, ¢ -> ¢.gc.drawPolygon(outline));

		if (locX == Integer.MIN_VALUE)
			return;
		Point shellLoc = new Point(locX, locY);
		if ((anchor & SWT.BOTTOM) != 0)
			shellLoc.y = shellLoc.y - shellSize.y + 1;
		if ((anchor & SWT.LEFT) != 0)
			shellLoc.x -= 15;
		else if ((anchor & SWT.RIGHT) != 0)
			shellLoc.x = shellLoc.x - shellSize.x + 16;
		if (autoAnchor) {
			if (shellLoc.x < screen.x)
				shellLoc.x = screen.x;
			else if (shellLoc.x > screen.x + screen.width - shellSize.x)
				shellLoc.x = screen.x + screen.width - shellSize.x;
			if (anchor == SWT.NONE)
				if (shellLoc.y < screen.y)
					shellLoc.y = screen.y;
				else if (shellLoc.y > screen.y + screen.height - shellSize.y)
					shellLoc.y = screen.y + screen.height - shellSize.y;
		}
		shell.setLocation(shellLoc);
	}

	void open() {
		prepareForOpen();
		shell.open();
	}

	void close() {
		if (! shell.isDisposed())
			shell.close();
	}

	// void setVisible(boolean visible) {
	//	if (visible) prepareForOpen();
	//	shell.setVisible(visible);
	// }

	private static int[] createOutline(Point size, int anchor, boolean outer) {
		int o = outer ? 1 : 0;
		int w = size.x + o;
		int h = size.y + o;

		switch(anchor) {
			case SWT.RIGHT|SWT.BOTTOM:
				return new int[] {
					// top and top right
					5, 0, w-6, 0, w-6, 1, w-4, 1, w-4, 2, w-3, 2, w-3, 3, w-2, 3, w-2, 5, w-1, 5,
					// right and bottom right
					w-1, h-26, w-2, h-26, w-2, h-24, w-3, h-24, w-3, h-23, w-4, h-23, w-4, h-22, w-6, h-22, w-6, h-21,
					// bottom with anchor
					w-16, h-21, w-16, h-1, w - o - 16, h-1, w - o - 16, h-2, w - o - 17, h-2, w - o - 17, h-3, w - o - 18, h-3, w - o - 18, h-4,
					w - o - 19, h-4, w - o - 19, h-5, w - o - 20, h-5, w - o - 20, h-6, w - o - 21, h-6, w - o - 21, h-7, w - o - 22, h-7, w - o - 22, h-8,
					w - o - 23, h-8, w - o - 23, h-9, w - o - 24, h-9, w - o - 24, h-10, w - o - 25, h-10, w - o - 25, h-11, w - o - 26, h-11,
					w - o - 26, h-12, w - o - 27, h-12, w - o - 27, h-13, w - o - 28, h-13, w - o - 28, h-14, w - o - 29, h-14, w - o - 29, h-15,
					w - o - 30, h-15, w - o - 30, h-16, w - o - 31, h-16, w - o - 31, h-17, w - o - 32, h-17, w - o - 32, h-18, w - o - 33, h-18,
					w - o - 33, h-19, w - o - 34, h-19, w - o - 34, h-20, w - o - 35, h-20, w - o - 35, h-21,
					// bottom left
					5, h-21, 5, h-22, 3, h-22, 3, h-23, 2, h-23, 2, h-24, 1, h-24, 1, h-26, 0, h-26,
					// left and top left
					0, 5, 1, 5, 1, 3, 2, 3, 2, 2, 3, 2, 3, 1, 5, 1
				};
			case SWT.LEFT|SWT.BOTTOM:
				return new int[] {
					// top and top right
					5, 0, w-6, 0, w-6, 1, w-4, 1, w-4, 2, w-3, 2, w-3, 3, w-2, 3, w-2, 5, w-1, 5,
					// right and bottom right
					w-1, h-26, w-2, h-26, w-2, h-24, w-3, h-24, w-3, h-23, w-4, h-23, w-4, h-22, w-6, h-22, w-6, h-21,
					// bottom with anchor
					34+o, h-21, 34+o, h-20, 33+o, h-20, 33+o, h-19, 32+o, h-19, 32+o, h-18, 31+o, h-18, 31+o, h-17,
					30+o, h-17, 30+o, h-16, 29+o, h-16, 29+o, h-15, 28+o, h-15, 28+o, h-14, 27+o, h-14, 27+o, h-13,
					26+o, h-13, 26+o, h-12, 25+o, h-12, 25+o, h-11, 24+o, h-11, 24+o, h-10, 23+o, h-10, 23+o, h-9,
					22+o, h-9, 22+o, h-8, 21+o, h-8, 21+o, h-7, 20+o, h-7, 20+o, h-6, 19+o, h-6, 19+o, h-5, 18+o, h-5,
					18+o, h-4, 17+o, h-4, 17+o, h-3, 16+o, h-3, 16+o, h-2, 15+o, h-2, 15, h-1, 15, h-21,
					// bottom left
					5, h-21, 5, h-22, 3, h-22, 3, h-23, 2, h-23, 2, h-24, 1, h-24, 1, h-26, 0, h-26,
					// left and top left
					0, 5, 1, 5, 1, 3, 2, 3, 2, 2, 3, 2, 3, 1, 5, 1
				};
			case SWT.TOP | SWT.RIGHT:
				return new int[] {
					// top with anchor
					5, 20, w - o - 35, 20, w - o - 35, 19, w - o - 34, 19, w - o - 34, 18, w - o - 33, 18, w - o - 33, 17, w - o - 32, 17, w - o - 32, 16,
					w - o - 31, 16, w - o - 31, 15, w - o - 30, 15, w - o - 30, 14, w - o - 29, 14, w - o - 29, 13, w - o - 28, 13, w - o - 28, 12,
					w - o - 27, 12, w - o - 27, 11, w - o - 26, 11, w - o - 26, 10, w - o - 25, 10, w - o - 25, 9, w - o - 24, 9, w - o - 24, 8, w - o - 23, 8,
					w - o - 23, 7, w - o - 22, 7, w - o - 22, 6, w - o - 21, 6, w - o - 21, 5, w - o - 20, 5, w - o - 20, 4, w - o - 19, 4, w - o - 19, 3,
					w - o - 18, 3, w - o - 18, 2, w - o - 17, 2, w - o - 17, 1, w - o - 16, 1, w - o - 16, 0, w-16, 0, w-16, 20,
					// top and top right
					w-6, 20, w-6, 21, w-4, 21, w-4, 22, w-3, 22, w-3, 23, w-2, 23, w-2, 25, w-1, 25,
					// right and bottom right
					w-1, h-6, w-2, h-6, w-2, h-4, w-3, h-4, w-3, h-3, w-4, h-3, w-4, h-2, w-6, h-2, w-6, h-1,
					// bottom and bottom left
					5, h-1, 5, h-2, 3, h-2, 3, h-3, 2, h-3, 2, h-4, 1, h-4, 1, h-6, 0, h-6,
					// left and top left
					0, 25, 1, 25, 1, 23, 2, 23, 2, 22, 3, 22, 3, 21, 5, 21
				};
			case SWT.TOP | SWT.LEFT:
				return new int[] {
					// top with anchor
					5, 20, 15, 20, 15, 0, 15+o, 0, 16+o, 1, 16+o, 2, 17+o, 2, 17+o, 3, 18+o, 3, 18+o, 4, 19+o, 4, 19+o, 5,
					20+o, 5, 20+o, 6, 21+o, 6, 21+o, 7, 22+o, 7, 22+o, 8, 23+o, 8, 23+o, 9, 24+o, 9, 24+o, 10, 25+o, 10,
					25+o, 11, 26+o, 11, 26+o, 12, 27+o, 12, 27+o, 13, 28+o, 13, 28+o, 14, 29+o, 14, 29+o, 15, 30+o, 15,
					30+o, 16, 31+o, 16, 31+o, 17, 32+o, 17, 32+o, 18, 33+o, 18, 33+o, 19, 34+o, 19, 34+o, 20,
					// top and top right
					w-6, 20, w-6, 21, w-4, 21, w-4, 22, w-3, 22, w-3, 23, w-2, 23, w-2, 25, w-1, 25,
					// right and bottom right
					w-1, h-6, w-2, h-6, w-2, h-4, w-3, h-4, w-3, h-3, w-4, h-3, w-4, h-2, w-6, h-2, w-6, h-1,
					// bottom and bottom left
					5, h-1, 5, h-2, 3, h-2, 3, h-3, 2, h-3, 2, h-4, 1, h-4, 1, h-6, 0, h-6,
					// left and top left
					0, 25, 1, 25, 1, 23, 2, 23, 2, 22, 3, 22, 3, 21, 5, 21
				};
			default:
				return new int[] {
					// top and top right
					5, 0, w-6, 0, w-6, 1, w-4, 1, w-4, 2, w-3, 2, w-3, 3, w-2, 3, w-2, 5, w-1, 5,
					// right and bottom right
					w-1, h-6, w-2, h-6, w-2, h-4, w-3, h-4, w-3, h-3, w-4, h-3, w-4, h-2, w-6, h-2, w-6, h-1,
					// bottom and bottom left
					5, h-1, 5, h-2, 3, h-2, 3, h-3, 2, h-3, 2, h-4, 1, h-4, 1, h-6, 0, h-6,
					// left and top left
					0, 5, 1, 5, 1, 3, 2, 3, 2, 2, 3, 2, 3, 1, 5, 1
				};
		}
	}

	private static Image createCloseImage(Display d, Color bg, Color fg) {
		int size = 11, off = 1;
		Image $ = new Image(d, size, size);
		GC gc = new GC($);
		gc.setBackground(bg);
		gc.fillRectangle($.getBounds());
		gc.setForeground(fg);
		gc.drawLine(off, off, size - off - 1, size - off - 1);
		gc.drawLine(1+off, off, size - off - 1, size - off - 2);
		gc.drawLine(off, 1+off, size - off - 2, size - off - 1);
		gc.drawLine(size - off - 1, off, off, size - off - 1);
		gc.drawLine(size - off - 1, 1+off, 1+off, size - off - 1);
		gc.drawLine(size - off - 2, off, off, size - off - 2);
		/*gc.drawLine(1, 0, size-2, 0);
		gc.drawLine(1, size-1, size-2, size-1);
		gc.drawLine(0, 1, 0, size-2);
		gc.drawLine(size-1, 1, size-1, size-2);*/
		gc.dispose();
		return $;
	}
}
