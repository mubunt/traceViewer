//------------------------------------------------------------------------------
// Copyright (c) 2014-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.apache.commons.cli.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

class traceViewer {
	//--- APPLICATION ----------------------------------------------------------
	static final String sTitle = "Trace Viewer Application";
	private static final String sPropertiesSuffix = ".properties";
	private static String sProperties = ".traceViewerGui" + sPropertiesSuffix;
	//--- PROPERTIES -----------------------------------------------------------
	private static final String propertyStringDelimiter = ",";
	private static final String propertyApplicationName = "_ApplicationName";
	private static final String propertyApplicationVers = "_ApplicationVersion";
	private static final String propertyApplicationCopy = "_ApplicationCopyright";
	private static final String propertyApplicationHelp = "_ApplicationHelp";
	private static final String propertyColumnNames = "_ColumsName";
	private static final String propertyColumnWidths = "_ColumsWidth";
	private static final String propertyTraceFileSuffix = "_Suffix";
	private static final String propertyFileDecoderExe = "_TraceDecoderExe";
	private static final String propertyFileDecoderExeArg = "_TraceDecoderArg";
	private static final String columnLineNumber = "#";
	private static final String columnLineNumberWidth = "60";
	private static final String property_applicationNameDefault = sTitle;
	private static final String property_applicationVersDefault = "Unknown";
	private static final String property_applicationCopyDefault = "Copyright (c) 2014-2016, 2020, Michel RIZZO.\n"
					+ "\n"
					+ "This program is free software; you can redistribute it and/or\n"
					+ "modify it under the terms of the GNU General Public License\n"
					+ "as published by the Free Software Foundation; either version 2\n"
					+ "of the License, or (at your option) any later version.\n";
	private static final String property_columnNamesDefault = "MasterID, Channel, Sequence, Level, Information";
	private static final String property_columnWidthsDefault = "100, 70, 80, 100, 300";
	private static final String property_TraceDecoderNameDefault = "myDecoder";
	private static final String property_TraceDecoderArgDefault = "--file=%f %m";
	private static final String property_FileSuffixDefault = "trc";
	private static final String property_applicationHelpDefault = "Any help will be welcome....";
	private static final String intelliJobjpath = "traceViewer_java/traceViewer/out/production/traceViewer/";

	static String applicationName;
	static String applicationVers;
	static String applicationCopy;
	static String applicationHelp;
	static String[]	columnNames	= { "" };
	static int[] columnWidths;
	static String traceFileSuffix;
	static String traceFiledecoder;
	static String traceFiledecoderarg;
	static boolean verboseMode;
	static boolean notifyMode;
	//---- MARKERS -------------------------------------------------------------
	static final int maxMarker = 100;
	static final int markerWidth = 10;
	static final int markerHeaderHeight = 24;
	//---- COLORS and FONTS ----------------------------------------------------
	private static final Color myDarkGray = new Color(Display.getDefault(), 64, 64, 64);
	private static final Color myDarkGreen = new Color(Display.getDefault(), 0, 41, 0);
	private static final Color myLightGreen = new Color(Display.getDefault(), 0, 230, 0);
	private static final Color myLedGreen = new Color(Display.getDefault(), 200, 246, 200);
	private static final Color myDarkYellow = new Color(Display.getDefault(), 255, 215, 0);

	static final Color ColorMarkedLine = myDarkYellow;
	static final Color ColorMarkedTrace = Display.getDefault().getSystemColor(SWT.COLOR_BLACK);
	static final Color ColorShell = myDarkGray;
	static final Color ColorLed = myLedGreen;
	static final Color ColorTrace = Display.getDefault().getSystemColor(SWT.COLOR_WHITE);
	static final Color consoleBackground = myDarkGreen;
	static final Color consoleForeground = myLightGreen;

	private static final Font fontTitle = new Font(Display.getDefault(), "Arial", 14, SWT.BOLD);
	static final Font fontText = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
	static final Font fontConsole = new Font(Display.getCurrent(), "Arial", 9, SWT.NORMAL);

	private final static String slash = System.getProperty("file.separator");
	static final String eol = System.getProperty("line.separator");
	static final String homedir = System.getProperty("user.home");
	//--------------------------------------------------------------------------
	public static void main(String[] args) {
		// Path of this file
		String PropertyPath = traceViewer.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		PropertyPath = PropertyPath.replace("traceViewer.jar", "");
		if (PropertyPath.length() > intelliJobjpath.length()) {
			String s = PropertyPath.substring(PropertyPath.length() - intelliJobjpath.length());
			if (s.equals(intelliJobjpath)) {
				PropertyPath = PropertyPath.substring(0, PropertyPath.length() - intelliJobjpath.length() - 1);
				PropertyPath += slash + "tests" + slash;
			}
		}
		//----------------------------------------------------------------------
		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",       false, "print this message and exit");
		options.addOption("V", "version",    false, "print the version information and exit");
		options.addOption("p", "prop",       true,  "basename of the property file to be used");
		options.addOption("v", "verbose",    false, "verbose mode");
		options.addOption("n", "notify",     false, "notify when trace decoding completed");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitle, options);
				Exit(0);
			}
			// Option: Version
			if (line.hasOption("version")) {
				System.out.println(sTitle + " (TVA)" +
				 	"	Version: " + traceViewerBuildInfo.getVersion() +
					" - Build: " + traceViewerBuildInfo.getNumber() +
					" - Date: " + traceViewerBuildInfo.getDate());
				Exit(0);
			}
			// Option: Property
			if (line.hasOption("prop"))
				sProperties = line.getOptionValue("p") + sPropertiesSuffix;
			// Option: Verbose
			if (line.hasOption("verbose"))
				verboseMode = true;
			// Option: Notification
			if (line.hasOption("notify"))
				notifyMode = true;
		} catch (ParseException exp) { Error("Parsing failed - " + exp.getMessage()); }
		//----------------------------------------------------------------------
		// APPLICATION PROPERTIES
		getAppliProperties(PropertyPath);
		//----------------------------------------------------------------------
		// GO ON....
		traceViewerGui.TraceViewer();
		Exit(0);
	}
	//--------------------------------------------------------------------------
	private static void getAppliProperties(String PropertyPath) {
		Properties properties = new Properties();
		String propertiesfile = PropertyPath + sProperties;
		File f = new File(propertiesfile);
		// Define and save the properties
		if(! f.isFile()) {
			if (verboseMode) traceViewerGui.writeConsole("Creating property file " + propertiesfile + eol);
			properties.setProperty(propertyApplicationName, property_applicationNameDefault);
			properties.setProperty(propertyApplicationVers, property_applicationVersDefault);
			properties.setProperty(propertyApplicationCopy, property_applicationCopyDefault);
			properties.setProperty(propertyApplicationHelp, property_applicationHelpDefault);
			properties.setProperty(propertyColumnNames, property_columnNamesDefault);
			properties.setProperty(propertyColumnWidths, property_columnWidthsDefault);
			properties.setProperty(propertyTraceFileSuffix, property_FileSuffixDefault);
			properties.setProperty(propertyFileDecoderExe, property_TraceDecoderNameDefault);
			properties.setProperty(propertyFileDecoderExeArg, property_TraceDecoderArgDefault);
			FileOutputStream out;
			try {
				out = new FileOutputStream(propertiesfile);
				properties.store(out, null);
				out.close();
			} catch (IOException e) { e.printStackTrace(); }
		}
		// Load the properties file
		if (verboseMode) traceViewerGui.writeConsole("Loading property file " + propertiesfile + eol);
		FileInputStream in;
		try {
			in = new FileInputStream(propertiesfile);
			properties.load(in);
			in.close();
		} catch (IOException e) { e.printStackTrace(); }
		// Convert properties.....
		// ...Application
		applicationName = properties.getProperty(propertyApplicationName, property_applicationNameDefault);
		applicationVers = properties.getProperty(propertyApplicationVers, property_applicationVersDefault);
		applicationCopy = properties.getProperty(propertyApplicationCopy, property_applicationCopyDefault);
		applicationHelp = properties.getProperty(propertyApplicationHelp, property_applicationHelpDefault);
		// ...Column names
		String tmp = columnLineNumber + propertyStringDelimiter + properties.getProperty(propertyColumnNames, "");
		columnNames = tmp.split(propertyStringDelimiter);
		for (int z = 0; z < columnNames.length; ++z) columnNames[z] = columnNames[z].trim();
		// ...Column widths
		tmp = columnLineNumberWidth + propertyStringDelimiter + properties.getProperty(propertyColumnWidths, "");
		String[] tmpw = tmp.split(propertyStringDelimiter);
		columnWidths = new int[tmpw.length];
		for (int i = 0; i < tmpw.length; ++i)
			try {
				columnWidths[i] = Integer.parseInt(tmpw[i].trim());
			} catch (NumberFormatException e) {
				System.out.println("NumberFormatException: " + e.getMessage());
			}
		// ...Trace decoder
		traceFileSuffix = properties.getProperty(propertyTraceFileSuffix, property_FileSuffixDefault);
		traceFiledecoderarg = properties.getProperty(propertyFileDecoderExeArg, property_TraceDecoderArgDefault);
		traceFiledecoder = expandEnvVars(properties.getProperty(propertyFileDecoderExe, property_TraceDecoderNameDefault));
		if (! traceFiledecoder.contains("/")) {
			String s = isCommandAvailable(traceFiledecoder);
			if (s.length() != 0) traceFiledecoder = s;
		}
	}

	private static String isCommandAvailable(String executableFileName) {
		final String ENVIRONMENT_VARIABLES_TEXT = System.getenv("PATH");
		String[] environmentVariables = ENVIRONMENT_VARIABLES_TEXT.split(File.pathSeparator);
		for (String environmentVariable : environmentVariables) {
			try {
				Path environmentVariablePath = Paths.get(environmentVariable);
				if (Files.exists(environmentVariablePath)) {
					Path resolvedEnvironmentVariableFilePath = environmentVariablePath.resolve(executableFileName);
					if (Files.isExecutable(resolvedEnvironmentVariableFilePath)) {
						return resolvedEnvironmentVariableFilePath.toString();
					}
				}
			} catch (InvalidPathException exception) {
				exception.printStackTrace();
			}
		}
		return "";
	}

	//--------------------------------------------------------------------------
	private static void Error(String s) {
		System.err.println("!!! ERROR !!! " + sTitle + ": " + s);
		Exit(-1);
	}
	//--------------------------------------------------------------------------
	static void Exit(int value) {
		fontText.dispose();
		fontTitle.dispose();
		fontConsole.dispose();
		myDarkYellow.dispose();
		myDarkGray.dispose();
		myDarkGreen.dispose();
		myLightGreen.dispose();
		myLedGreen.dispose();
		System.exit(value);
	}
	//--------------------------------------------------------------------------
	private static String expandEnvVars(String text) {
		Map<String, String> envMap = System.getenv();
		for (Entry<String, String> z : envMap.entrySet())
			text = text.replaceAll("\\$\\{" + z.getKey() + "\\}", z.getValue());
		return text;
	}
}
//==============================================================================
