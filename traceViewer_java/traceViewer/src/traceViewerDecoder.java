//------------------------------------------------------------------------------
// Copyright (c) 2014-2017, 2019, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.exec.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;

import java.io.IOException;

class traceViewerDecoder {
	private static final int notificationTimeout = 30000;	// Timeout for notification in milliseconds
	private static int screenWidth;
	private static int screenHeight;
	//======================================================================
	static void runTraceDecoderExe() {
		//final long runJobTimeout = 15000;
		final long runJobTimeout = 0;
		ExecCommandHandler runresult = null;
		final CommandLine decodecmd;
		String[] temp;
		String decoder, decoderarg;

		decoderarg =  traceViewer.traceFiledecoderarg;
		if (decoderarg.contains("%f") || decoderarg.contains("%m")) {
			if (!traceViewerGui.traceDecoderOption()) return;
			if (decoderarg.contains("%f") && traceViewerGui.DataFile != null)
				decoderarg = decoderarg.replaceAll("%f", traceViewerGui.DataFile);
			if (decoderarg.contains("%m") && traceViewerGui.MoreOptions != null)
				decoderarg = decoderarg.replaceAll("%m", traceViewerGui.MoreOptions);
		}
		decodecmd = new CommandLine(traceViewer.traceFiledecoder);
		decoder = traceViewer.traceFiledecoder;
		temp = decoderarg.split(" ");
		for (String aTemp : temp) decodecmd.addArgument(aTemp);
		try {
			if (traceViewer.verboseMode)
				traceViewerGui.writeConsole("Preparing trace decoding job: " + decoder + " " + decoderarg + traceViewer.eol);
			runresult = runDecoder(decodecmd, runJobTimeout);
		} catch (final Exception e) {
			traceViewerGui.writeConsole("The trace decoding job failed !!! (\"" + decoder + " " + decoderarg + "\")" + traceViewer.eol);
		}
		if (traceViewer.verboseMode)
			traceViewerGui.writeConsole("Decoding is running. Waiting for the job to finish..." + traceViewer.eol);
		try {
			assert runresult != null;
			runresult.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Display.getDefault().asyncExec(() -> {
            int n = traceViewerNumberOfLine.get();
            traceViewerGui.writeConsole("Number of decoded lines: " + n + traceViewer.eol);
			traceViewerGui.writeCounter(n);
            traceViewerGui.drawMarkers(n);
            if (traceViewer.notifyMode)
                BalloonNotify();
        });
	}
	//--------------------------------------------------------------------------
	private static ExecCommandHandler runDecoder(final CommandLine l, final long runJobTimeout)
			throws IOException {
		ExecuteWatchdog watchdog = null;
		ExecCommandHandler $;

		// Create the executor and consider the exitValue '0' as success
		final Executor executor = new DefaultExecutor();
		executor.setExitValue(0);

		// Create a watchdog if requested
		if (runJobTimeout > 0) {
			watchdog = new ExecuteWatchdog(runJobTimeout);
			executor.setWatchdog(watchdog);
		}

		LogOutputStream output = new LogOutputStream() {
			@Override protected void processLine(String line, int level) {
				AddOneLine(line);
			}
		};
		LogOutputStream errput = new LogOutputStream() {
			@Override protected void processLine(String line, int level) {
				traceViewerGui.writeConsole(line + traceViewer.eol);
			}
		};

		PumpStreamHandler streamHandler = new PumpStreamHandler(output, errput);
		executor.setStreamHandler(streamHandler);

		$ = new ExecCommandHandler(watchdog);
		executor.execute(l, $);
		return $;
	}
	//--------------------------------------------------------------------------
	private static void AddOneLine(String s) {
		Display.getDefault().asyncExec(() -> {
            if (traceViewerGui.VIEWER.isDisposed()) return;
            TableItem item = new TableItem(traceViewerGui.VIEWER, SWT.NONE);
            item.setText(0, traceViewerNumberOfLine.get() + "");
            String delimiter = "\\|";
            String[] temp = s.split(delimiter);
            for (int z = 0; z < temp.length ; ++z)
                if (z < traceViewer.columnNames.length)
                    item.setText(z + 1, temp[z]);
            traceViewerNumberOfLine.increment();
            int n = traceViewerNumberOfLine.get();
			if (n % 5 == 0)
                traceViewerGui.writeCounter(n);
            if (n % 100 == 0)
                traceViewerGui.drawMarkers(n);
        });
	}
	//==========================================================================
	private static void BalloonNotify() {
		getScreenSize(Display.getCurrent());
		balloonWindow balloon = new balloonWindow(traceViewerGui.SHELL, SWT.TOOL | SWT.CLOSE | SWT.TITLE | SWT.ON_TOP);
		balloon.setText(traceViewer.applicationName);
		Composite c = balloon.getContents();
		c.setLayout(new FillLayout());
		Label l = new Label(c, SWT.NONE);
		l.setText("Decoding and displaying traces completed!");
		c.pack(true);
		// Locate the balloon to the widget location
		balloon.setLocation(screenWidth, screenHeight);
		// Runnable that will close the window after time has been expired
		balloon.getShell().getDisplay().timerExec(notificationTimeout, balloon::close);
		balloon.open();
	}
	//--------------------------------------------------------------------------
	private static void getScreenSize(Display d) {
		final Monitor monitor = d.getPrimaryMonitor();
		final Rectangle rect = monitor != null ? monitor.getClientArea() : Display.getCurrent().getBounds();
		screenWidth = rect.width;
		screenHeight = rect.height;
	}
	//==========================================================================
	static class ExecCommandHandler extends DefaultExecuteResultHandler {
		private final ExecuteWatchdog watchdog;

		ExecCommandHandler(final ExecuteWatchdog watchdog) {
			this.watchdog = watchdog;
		}
		@Override
		public void onProcessComplete(final int exitValue) {
			super.onProcessComplete(exitValue);
			traceViewerGui.writeConsole("Traces were successfully decoded. Exit status: " + exitValue + traceViewer.eol);
		}
		@Override
		public void onProcessFailed(final ExecuteException e) {
			super.onProcessFailed(e);
			traceViewerGui.writeConsole("The trace decoding process " + (watchdog != null && watchdog.killedProcess()
					? "timed out" + traceViewer.eol : "failed to do: " + e.getCause().getMessage() + traceViewer.eol));
		}
	}
}
//==============================================================================
