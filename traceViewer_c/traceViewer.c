//------------------------------------------------------------------------------
// Copyright (c) 2017, 2019, Michel RIZZO (the 'author' in the following)
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef LINUX
#include <sys/wait.h>
#else
#include <windows.h>
#include <winbase.h>
#endif
//------------------------------------------------------------------------------
// MACROS DEFINITIONS TO CUSTOMIZE THE PROGRAM
//------------------------------------------------------------------------------
#define BINDIR					"/traceViewer_bin/"
#define TRACEVIEWER 			"traceViewer.jar"
#define HEADER					"traceViewer_cmdline.h"
#define PARSER 					cmdline_parser_traceViewer
#define FREE 					cmdline_parser_traceViewer_free
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include HEADER
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define DISPLAY 				"DISPLAY"
#define JAVA 					"java"
#ifdef LINUX
#define ProcessPseudoFileSystem	"/proc/self/exe"
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define error(fmt, ...) 		do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
#define EXIST(x)				(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)				(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static char jarpath[PATH_MAX];
static struct stat locstat, *ptlocstat;        // stat variables for EXIST*
static size_t	idx = 0;
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static bool testJARfile(char *jarFile) {
	char jarfile[PATH_MAX];
	snprintf(jarfile, sizeof(jarfile), "%s%s",jarpath, jarFile);
	if (! EXIST(jarfile)) {
		error("WRONG INSTALLATION - Jar file '%s' does not exist.", jarfile);
		return false;
	}
	return true;
}
static char *getJARfile( const char *jarFileList, char *res ) {
	char *pts = res;
	while (jarFileList[idx] != ' ' && jarFileList[idx] != '\0') {
		*pts = jarFileList[idx++];
		++pts;
	}
	*pts = '\0';
	if (jarFileList[idx] == ' ') ++idx;
	return res;
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Return code (EXIT_SUCCESS - Normal program stop / EXIT_FAILURE - Program stop on error)
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	char jarfile[PATH_MAX];
	char command[4096 * 4];
	char *pt;
	unsigned int i;
	int j, retval = 0;
	size_t k;
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	//---- Parameter checking and setting --------------------------------------
#ifdef LINUX
	if (readlink(ProcessPseudoFileSystem, jarpath, sizeof(jarpath)) == -1) {
#else
	if (0 == GetModuleFileName(NULL, jarpath, sizeof(jarpath))) {
#endif
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}

	dirname(jarpath);
	strcat(jarpath, BINDIR);
	if (! EXISTDIR(jarpath)) {
		error("WRONG INSTALLATION - Directory '%s' does not exist.", jarpath);
		return EXIT_FAILURE;
	}
	if (! testJARfile((char *)TRACEVIEWER)) return EXIT_FAILURE;
	char *ptlib = getJARfile(JARLIBS, jarfile);
	while (strlen(ptlib) != 0) {
		if (! testJARfile(ptlib)) return EXIT_FAILURE;
		ptlib = getJARfile(JARLIBS, jarfile);
	}
	//---- Parameter checking and setting --------------------------------------
	if (PARSER(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Run Java
	sprintf(command, "%s -cp %s -jar %s%s", JAVA, jarpath, jarpath, TRACEVIEWER);
	for (j = 1; j < argc; j++) {
		strcat(command, " ");
		pt = argv[j];
		k = strlen(command);
		while (*pt != '\0') {
			if (*pt == ' ') command[k++] = '\\';
			command[k] = *pt;
			command[++k] = '\0';
			++pt;
		}
	}
	retval = system(command);
	//---- End -------------------------------------------------------------------
	FREE(&args_info);
	return(retval);
}
//------------------------------------------------------------------------------
