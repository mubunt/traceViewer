//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
//------------------------------------------------------------------------------
// CONSTANTS
//------------------------------------------------------------------------------
#define MAX	10000
#define SEQ	25
//------------------------------------------------------------------------------
// ROUTINES
//------------------------------------------------------------------------------
int rand_n(int n) {
	int partSize = 1 + (n == RAND_MAX ? 0 : (RAND_MAX - n) / (n + 1));
	int maxUsefull = partSize * n + (partSize - 1);
	int draw;
	do {
		draw = rand();
	} while (draw > maxUsefull);
	return draw / partSize;
}
//==============================================================================
// MAIN
//==============================================================================
int main ( int argc, char *argv[] ) {
	int i;
	fprintf(stderr, "In this test case, the parameters only serve to demonstrate the syntax\nof a decoder and do not intervene in the generation of traces.\n\n");
	fprintf(stderr, "Number of arguments: %d\n", argc);
	for (i = 0 ; i< argc ; i ++) {
		fprintf(stderr, "argv[%d] : ‘%s’\n", i, argv[i]);
	}

	char level[3][12] = { "Warning", "Information", "Severe" };
	char module[4][32] = { "main.c", "transmitter.c", "calibrator.c", "dualport.c" };
	for (i = 0; i < MAX; i++) {
		int core = rand_n(4);
		fprintf(stdout, "core %d|%d|%d|%s|line %d in %s\n", core, 100 + core, rand_n(SEQ), level[rand_n(2)], i, module[rand_n(3)]);
	}
	fprintf(stderr, "Generated %d lines of traces.\n", i);
	return 0;
}